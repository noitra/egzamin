from django.conf.urls import patterns, url
import views
from django.views.generic import TemplateView

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^dodaj$', views.form, name='form'),
    url(r'^chatuj$', views.dodajwiadomosc, name='chatuj'),
    url(r'^wiadomosci$', views.messages, name='msgs'),
    url(r'^zaloguj$', views.login, name='login'),
    url(r'^wyloguj$', views.logout, name='logout'),
)